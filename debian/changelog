libarchive-any-perl (0.0946-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Joenio Costa ]
  * Import upstream version 0.0946
  * refresh compile-tests-modules.patch
  * debian/watch: use uscan version 4
  * update upstream copyright name and year
  * debhelper-compat = 12
  * declare compliance with Debian Policy 4.4.0

  [ gregor herrmann ]
  * Drop libmodule-build-perl from Build-Depends.
    Build.PL is removed, and the distribution now uses ExtUtils::MakeMaker.

 -- Joenio Costa <joenio@joenio.me>  Fri, 19 Jul 2019 22:28:58 -0300

libarchive-any-perl (0.0945-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.0945
  * Refresh our-ISA.patch patch
  * Rename our-ISA.patch to compile-tests-modules.patch.
    After upstream having applied parts of the original submitted patch the
    only remaining part is to test use of Archive::Any::{Tar,Zip} in the
    testsuite. Keep this additional patch for now although test covered by
    the autopkgtest testsuite.

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 03 May 2016 08:07:36 +0200

libarchive-any-perl (0.0944-1) unstable; urgency=medium

  * Team upload.
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI
  * Add debian/upstream/metadata
  * Import upstream version 0.0944
  * Bump Debhelper compat level to 9
  * Update copyright years for upstream files
  * Declare compliance with Debian policy 3.9.8
  * Refresh our-ISA.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 02 May 2016 08:53:29 +0200

libarchive-any-perl (0.0942-2) unstable; urgency=medium

  * Team upload.
  * Add explicit build dependency on libmodule-build-perl

 -- Niko Tyni <ntyni@debian.org>  Sat, 06 Jun 2015 20:56:21 +0300

libarchive-any-perl (0.0942-1) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ernesto Hernández-Novich (USB) ]
  * Upgraded to DH8 using dh-make-perl refresh mode;
  * debian/control: upgraded to DH8; upgraded Standards-Version;
    removed versioned dependencies already provided in old-stable;
    set Debian Perl Group as Maintainer instead of me.
  * debian/copyright: upgraded to DEP format.
  * debian/compat: upgraded to DH8.
  * Upgraded to Source Format 3.0

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Damyan Ivanov ]
  * Imported Upstream version 0.0940
  * Update copyright (upstream author change)

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Imported Upstream version 0.0941

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Update debian/copyright format specification to 1.0 and remove
    line-break between key and value.

  [ Damyan Ivanov ]
  * Imported Upstream version 0.0942
  * bump year of upstream copyright
  * add patch fixing automatic package tests
  * add Testsuite header
  * drop 'perl' from B-D-I, as it is already in B-D
  * Declare conformance with Policy 3.9.6

 -- Damyan Ivanov <dmn@debian.org>  Fri, 29 May 2015 20:54:53 +0000

libarchive-any-perl (0.0932-1) unstable; urgency=low

  * Initial Release (Closes: #483704).
  * Included copyright clarification e-mail received from upstream
    author.

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Thu, 19 Jun 2008 15:44:16 -0430
